# PyVideo Data Exporter

Export videos from a Jekyll site's `_talks/` directory into the JSON
format used by http://pyvideo.org/.
