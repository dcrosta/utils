"""
Create or update a directory of JSON files for PyVideo.org from a
directory of Jekyll-style talk descriptions in Markdown format.

usage: see python pyvideo-export.py 

PYVIDEO_DIR should be the local checkout of the PyVideo data repository.
This will create/update a directory of files for the talks, e.g.
"$PYVIDEO_DIR/pygotham-2017/videos/".

This also assumes the presence of a file "videos.csv" in the working
directory which contains 3 columns youtube_video_id, youtube_url, and
talk_title. I generated this file by hand with a lot of open tabs in 
my browser. A copy of it exists in this directory, for reference.
"""
import argparse
import json
import os
import os.path
import sys

import frontmatter

import csv


parser = argparse.ArgumentParser(
    description="Export talks for PyVideo.org",
    epilog=(
        "The VIDEOS_CSV file must contain 2 columns: the YouTube video ID, "
        "and the title of the talk that ID corresponds to. It should contain "
        "no header row.\n\n"
        "The OUTPUT_DIR should be the directory you want to contain the JSON "
        "files itself, e.g. '/path/to/pyvideo/data/pygotham-2017/videos/'."
    ),
)
parser.add_argument("talks_dir", metavar="TALKS_DIR", help="Directory containing one .md file per talk")
parser.add_argument("video_csv", metavar="VIDEO_CSV", help="Mapping from YouTube video ID to talk title")
parser.add_argument("output_dir", metavar="OUTPUT_DIR", help="Directory into which to place the JSON output files")
opts = parser.parse_args()

with open(opts.video_csv) as fp:
    reader = csv.reader(fp)
    videos = {}
    for vid, url, title in reader:
        videos[title] = (vid, url)


for talk in os.listdir(opts.talks_dir):
    talkpath = os.path.join(opts.talks_dir, talk)
    with open(talkpath) as fp:
        parsed = frontmatter.load(fp)

    if parsed["type"].lower() != "talk":
        continue

    slug, _ = os.path.splitext(talk)

    vid, url = videos[parsed["title"]]

    doc = {
        "copyright_text": "Creative Commons Attribution license (reuse allowed)",
        "description": parsed.content,
        "duration": parsed["duration"] * 60,
        "language": "eng",
        "recorded": parsed["slot"].strftime("%Y-%m-%d"), # "2016-07-16",
        "related_urls": [
            "https://2017.pygotham.org/talks/%s/" % slug,
        ],
        "speakers": parsed["speakers"],
        "tags": [""],
        "thumbnail_url": "https://i.ytimg.com/vi/%s/maxresdefault.jpg" % vid,
        "title": parsed["title"],
        "videos": [
            {
                "type": "youtube",
                "url": url,
            }
        ]
    }

    filename = slug + ".json"
    outpath = os.path.join(opts.output_dir, filename)
    with open(outpath, "w") as fp:
        json.dump(doc, fp, indent=2)
