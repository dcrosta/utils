"""
Update YouTube titles and descriptions with data from ../_talks/.

Assumes that each of the talks already has a "video_url" attribute
formatted as a youtu.be short link.
"""
import argparse
import os.path

import frontmatter
import google.oauth2.credentials
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from google_auth_oauthlib.flow import InstalledAppFlow


parser = argparse.ArgumentParser(
    description="Update talk descriptions on YouTube",
    epilog=(
        "Before using this script, you will need to generate and download "
        "a client_secret.json file from the Google developer console. Go "
        "to https://console.developers.google.com/ using your PyGotham "
        "account. Select an appropriate project, then click 'Credentials' "
        "on the left rail. Click 'Create Credentials', then 'OAuth Client "
        "ID', then 'Other' and click through the prompts. Click the download "
        "link, and save the file as 'client_secret.json' in this directory."
    ),
)
parser.add_argument("talks_dir", metavar="TALKS_DIR", help="Directory containing one .md file per talk")
opts = parser.parse_args()


# The CLIENT_SECRETS_FILE variable specifies the name of a file that contains
# the OAuth 2.0 information for this application, including its client_id and
# client_secret.
CLIENT_SECRETS_FILE = "client_secret.json"

# This OAuth 2.0 access scope allows for full read/write access to the
# authenticated user's account and requires requests to use an SSL connection.
SCOPES = ['https://www.googleapis.com/auth/youtube.force-ssl']
API_SERVICE_NAME = 'youtube'
API_VERSION = 'v3'

def get_authenticated_service():
    flow = InstalledAppFlow.from_client_secrets_file(CLIENT_SECRETS_FILE, SCOPES)
    credentials = flow.run_console()
    return build(API_SERVICE_NAME, API_VERSION, credentials = credentials)


talks = []
for talk_file in os.listdir(opts.talks_dir):
    if not talk_file.endswith(".md"):
        continue

    talk_path = os.path.join(opts.talks_dir, talk_file)
    with open(talk_path) as fp:
        parsed = frontmatter.load(fp)

    url = parsed.get("video_url")
    if not url:
        continue

    # assume all URLs are youtu.be/<video_id>
    video_id = url.rsplit("/", 1)[-1]
    parsed["video_id"] = video_id

    talks.append(parsed)


service = get_authenticated_service()

for talk in talks:
    video_id = talk["video_id"]
    if video_id != "6gJKxe5zPXM":
        continue

    video = service.videos().list(
        id=video_id,
        part="snippet",
    ).execute()["items"][0]

    snippet = video["snippet"]

    speakers = talk.get("speakers")
    description = ""
    if len(speakers) == 1:
        description = "Speaker: %s\n\n" % speakers[0]
    elif speakers:
        description = "Speakers: %s\n\n" % ", ".join(speakers)

    description += str(talk)
    snippet["description"] = description

    service.videos().update(
        part="snippet",
        body=dict(
            id=video_id,
            snippet=snippet,
        ),
    ).execute()

