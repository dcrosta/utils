"""Import the schedule from a CSV file.

Usage: python import.py path_to_talks
"""

import collections
import csv
import datetime
import os
import sys

import frontmatter
from slugify import slugify

SPECIAL_SLOTS = {
    'Breakfast': 'meal',
    'Break': 'break',
    'Lunch': 'meal',
}


def main(csv_file_name: str, talk_directory_name: str) -> str:
    # Non-talks slots (e.g., meals) don't have unique title but need to
    # have unique filenames. We need to keep track of how many times
    # we've seen each kind of slot so that we can include a unique
    # number in the filename.
    counters = collections.defaultdict(int)

    with open(csv_file_name) as f:
        reader = csv.DictReader(f)

        for row in reader:
            slot = datetime.datetime.strptime(
                f"{row['Date']} {row['Time']} -0400",
                '%m/%d/%Y %I:%M:%S %p %z'
            )

            # TODO(dirn): The room names should be moved to header rows
            # in the CSV file.
            for room in ('PennTop South', 'PennTop North', 'Madison'):
                type_ = 'talk'

                title = row[room]
                if not title:
                    continue

                if title[:7] == 'Keynote':
                    type_ = 'keynote'


                slug = slugify(title)
                if title in SPECIAL_SLOTS:
                    type_ = SPECIAL_SLOTS[title]

                    counters[slug] += 1
                    slug = f'{slug}-{counters[slug]}'

                file_name = os.path.join(talk_directory_name, f'{slug}.md')

                talk = frontmatter.load(file_name)

                talk['slot'] = slot
                talk['room'] = room
                talk['type'] = type_

                # TODO(dirn): I believe I did this because the object
                # returned by frontmatter.load didn't support checking
                # for the presence of a key, but I should confirm that
                # and change this if it does.
                try:
                    talk['title']
                except KeyError:
                    talk['title'] = title

                with open(file_name, 'wb') as file_to_write:
                    frontmatter.dump(talk, file_to_write)

if __name__ == '__main__':
    main(sys.argv[1], sys.argv[2])
