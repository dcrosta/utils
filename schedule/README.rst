==================
Talk Time Importer
==================

This script can be used to import the times for each talk into its markdown
file.

The CSV file should be in the format::

    Date,Time,Room 1,Room 2,Room 3
    2017-10-06,08:00,Breakfast
    2017-10-06,09:00,Opening Remarks
    2017-10-06,10:00,A Talk,Another Talk,Yet Another Talk

The script can be run with::

    $ python import.py /path/to/site/_talks
